#!/bin/bash
#SBATCH --time=64:15:00

module load java/20.0 
module load singularity/3.9.7
module load awscli/2.0

if [ "$1" = "useSlurm" ]
then
    ./nextflow -log tmp/custom.log run main.nf -with-tower -profile slurm -bucket-dir 's3://nextflow-bucket-s3' --dataloc $2 --datadir $3 --outdir $4 -ansi-log false 
else
    ./nextflow -log tmp/custom.log run main.nf -with-tower -profile slurm -bucket-dir 's3://nextflow-bucket-s3' --dataloc $2 --datadir $3 --outdir $4 -ansi-log false -bg > log/my-file.log
fi
