#FROM nvcr.io/nvidia/clara/clara-parabricks:4.2.0-1
#FROM nvcr.io/nvidia/cloud-native/dcgm:3.3.3-1-ubuntu22.04
#FROM nvidia/cuda:12.3.2-cudnn9-devel-ubuntu22.04
#FROM nvidia/cuda:12.3.2-cudnn9-runtime-ubuntu22.04
#FROM nvcr.io/nvidia/pytorch:22.08-py3
#FROM nvcr.io/nvidia/l4t-pytorch:r35.2.1-pth2.0-py3 
FROM nvcr.io/nvidia/cuda:12.3.2-cudnn9-runtime-ubuntu22.04
#FROM nvcr.io/nvidia/pytorch:22.08-py3
#FROM nvcr.io/nvidia/pytorch:18.06-py3

WORKDIR /home/ec2-user
ENV RD2C=/home/ec2-user

RUN apt-get -y update && DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get install -y --allow-unauthenticated wget sudo procps python3-dev python3-pip vim git libxml2-dev libxslt-dev git\
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apt/archives/*

RUN cd ${RD2C} \
    && apt-get update \
    && apt-get install -y curl \
    && apt-get install -y zip \
    && apt-get install -y unzip \
    && curl -k "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && mkdir build \
    && sudo ./aws/install

ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES=compute,utility

RUN cd ${RD2C} \
    && mkdir prism \
    && mkdir ensemble \
    && mkdir compress \
    && mkdir compress/gpulz \
    && mkdir prism_input_files \
    && mkdir templates \
    && mkdir image_to_compress \
    && mkdir image_to_decompress \
    && mkdir decompress_images

RUN python3 -m pip install --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host=files.pythonhosted.org setuptools pip --upgrade \ 
    && python3 -m pip install --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host=files.pythonhosted.org pytorch-msssim compressai

COPY modules/prism/ressource/run_prism.py /home/ec2-user/prism/
COPY modules/prism/ressource/gen.py /home/ec2-user/prism/
COPY modules/compress/ressource/compress.py /home/ec2-user/compress/
COPY modules/compress/ressource/decompress.py /home/ec2-user/compress/
COPY modules/compress/ressource/compressGPU.py /home/ec2-user/compress/
COPY modules/compress/ressource/gpulz.so /home/ec2-user/compress/
COPY modules/compress/ressource/compressed.bin /home/ec2-user/compress/
COPY modules/ensemble/ressource/* /home/ec2-user/ensemble/
