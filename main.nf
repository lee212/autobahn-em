#!/usr/bin/env nextflow 

/*
 * Copyright (c) 2022, Seqera Labs.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This Source Code Form is "Incompatible With Secondary Licenses", as
 * defined by the Mozilla Public License, v. 2.0.
 *
 */
import groovy.json.JsonSlurper


include { analyze_h5 } from './modules/analyze_prism'
include { generate_xyz } from './modules/prism'
include { generate_h5 } from './modules/prism'
include { chip_sel } from './modules/ensemble'
include { ensemble_model } from './modules/ensemble'
include { train } from './modules/FENet'
include { compress } from './modules/compress'
include { uploadImagesAWS } from './modules/upload'
include { removeImagesAWS } from './modules/upload'
include { SAMIAM } from './modules/SamIAM'

nextflow.preview.recursion=true
nextflow.enable.dsl=2
log.info """
    M L - E X A M P L E   P I P E L I N E
    =====================================
    dataloc         : ${params.dataloc}
    datadir         : ${params.datadir}
    outdir          : ${params.outdir}
    """



/* 
 * main script flow SrTiO3
 */

def ch = Channel.value([1,2])

workflow stage_images {
    take:
    rr
    directory
    image_ID
    bucket

    main:
    uploadImagesAWS(rr, directory,image_ID, bucket)
 
    emit:
    uploadImagesAWS.out
}

workflow remove_images {
    take:
    rr
    bucket

    main:
    removeImagesAWS(rr, bucket)
}

workflow PRISM {
    take:
    chan
    cc
    rr
    API
    

    main:
    def dd = 0
    (x,y) = generate_xyz(chan, API, cc, rr)
    (w,nn) = generate_h5(chan, params.dataloc, x, y)
    (dd) = analyze_h5(chan, w, params.dataloc, x, nn, dd)
        
    emit:
    analyze_h5.out
}

workflow classification {
    take:
    out
 
    main:
    Channel.of("LiAl", "SrTiOGe").set{clas1}
    (r, name) = compress(out, clas1)
    train(r, name)
     
}

process get_time {
    input:
    val(start)
    val(end)

    script:
    """
    #!/bin/bash
    echo ${start}
    echo ${end}
    """

}

workflow {
    def round = 1
    def r=""
    Channel.of(1..4).set{chan}
    Channel.of(["Li,Al", "LiAl", "0"],["Sr,Ti,O,Ge", "SrTiOGe", "0"], ["Li,Al", "LiAl", "45"],["Sr,Ti,O,Ge", "SrTiOGe", "45"]).set{cc}
    def API="r8MGCmFV1NLtxZ1jNTQ1evr31dXI1C8R"

    /*(xx) = stage_images("1", params.datadir, "BiTE_1", params.dataloc) 
     */
    /*(oo) = PRISM(chan, cc , round, API)
     *oo.view{"value: $it"}
     *oo.branch {
     *   FALSE: it <= 3
     *          return false
     *   TRUE: it > 3
     *         return true
     *}.set{ch_if}
     */
    /*
     * Compress code
     */
    Channel.of("LiAl", "SrTiOGe").set{clas1}
    (r, name) = compress(round, clas1)
    
    /*remove_images( oo.collect(), params.dataloc) 
     */
    /*
     * Ensemble code
     */
     Channel.of("LiAl", "STO_GE_2").set{ID}
     Channel.of("background,precipitate", "STO,PtC,GE").set{mol}
     Channel.of("png", "jpg").set{ext}
    /*(chips, ww, mm, ext2) = chip_sel(round, ID, mol, ext)
     *(rr) = ensemble_model(chips, ww, mm, round, ext2)
     */
    SAMIAM(r, ext, ID) 
   
    /* 
     * oo.view{"value: $it"}    
     * oo.branch {
     *    FALSE: it <= 1
     *    TRUE: it > 1
     *  }.set{ch_if}
     * 
     * ch_if.TRUE | classification
     */
    
}

/* 
 * completion handler
 */
workflow.onComplete {
        log.info ( "$workflow.duration" )
	log.info ( workflow.success ? '\nDone!' : '\nOops .. something went wrong' )
}
