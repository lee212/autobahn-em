import numpy as np
import os
import time
import glob
import ssl
ssl._create_default_https_context = ssl._create_unverified_context
#from eli5.sklearn import PermutationImportance
from sklearn.inspection import permutation_importance
from keras.models import Model, load_model
from keras.models import Sequential
from keras.layers import Input, GaussianNoise, Embedding, BatchNormalization
from keras.layers import Dense, Conv2D,  MaxPool2D, Flatten, GlobalAveragePooling2D,  BatchNormalization, Layer, Add, Reshape
from keras.layers import Dense, Dropout, Activation, Flatten, TimeDistributed
from keras.layers import Conv2D,Conv1D, MaxPooling1D, LeakyReLU, PReLU
from keras.optimizers import RMSprop, Adam, SGD
from keras import applications
import tensorflow as tf
import pandas as pd
from keras.callbacks import EarlyStopping
from torchvision.transforms.functional import resize
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from torch.nn.utils.weight_norm import weight_norm
import torch
#import shap
import torch.nn as nn
from torch.nn import Softmax
import statistics
from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix
from keras.applications import ResNet50
from keras.applications import ResNet101
from keras.applications import ResNet152
#from keras.applications.resnet import preprocess_input
from keras.applications.vgg16 import VGG16 
#from keras.applications.vgg16 import preprocess_input 
from keras.applications.vgg19 import VGG19 
from keras.applications.vgg19 import preprocess_input 
from sklearn.decomposition import PCA
import seaborn as sns
import matplotlib.pyplot as plt
from torchvision.transforms.functional import resize
import torchvision.transforms as transforms
#from torchensemble import VotingClassifier
from sklearn.ensemble import VotingClassifier

#Start resnet18
class ResnetBlock(Model):
    """
    A standard resnet block.
    """

    def __init__(self, channels: int, down_sample=False):
        """
        channels: same as number of convolution kernels
        """
        super().__init__()

        self.__channels = channels
        self.__down_sample = down_sample
        self.__strides = [2, 1] if down_sample else [1, 1]

        KERNEL_SIZE = (3, 3)
        # use He initialization, instead of Xavier (a.k.a 'glorot_uniform' in Keras), as suggested in [2]
        INIT_SCHEME = "he_normal"

        self.conv_1 = Conv2D(self.__channels, strides=self.__strides[0],
                             kernel_size=KERNEL_SIZE, padding="same", kernel_initializer=INIT_SCHEME)
        self.bn_1 = BatchNormalization()
        self.conv_2 = Conv2D(self.__channels, strides=self.__strides[1],
                             kernel_size=KERNEL_SIZE, padding="same", kernel_initializer=INIT_SCHEME)
        self.bn_2 = BatchNormalization()
        self.merge = Add()

        if self.__down_sample:
            # perform down sampling using stride of 2, according to [1].
            self.res_conv = Conv2D(
                self.__channels, strides=2, kernel_size=(1, 1), kernel_initializer=INIT_SCHEME, padding="same")
            self.res_bn = BatchNormalization()

    def call(self, inputs):
        res = inputs

        x = self.conv_1(inputs)
        x = self.bn_1(x)
        x = tf.nn.relu(x)
        x = self.conv_2(x)
        x = self.bn_2(x)

        if self.__down_sample:
            res = self.res_conv(res)
            res = self.res_bn(res)

        # if not perform down sample, then add a shortcut directly
        x = self.merge([x, res])
        out = tf.nn.relu(x)
        return out


class ResNet18(Model):

    def __init__(self, num_classes, **kwargs):
        """
            num_classes: number of classes in specific classification task.
        """
        super().__init__(**kwargs)
        #self.conv_1 = Dense(64, activation="linear") #
        self.reshape = Reshape((10,10,1))
        self.conv_1 = Conv2D(64, (7, 7), strides=2,padding="same", kernel_initializer="he_normal")
        self.init_bn = BatchNormalization()
        self.pool_2 = MaxPool2D(pool_size=(2, 2), strides=2, padding="same")
        self.res_1_1 = ResnetBlock(64)
        self.res_1_2 = ResnetBlock(64)
        self.res_2_1 = ResnetBlock(128, down_sample=True)
        self.res_2_2 = ResnetBlock(128)
        self.res_3_1 = ResnetBlock(256, down_sample=True)
        self.res_3_2 = ResnetBlock(256)
        self.res_4_1 = ResnetBlock(512, down_sample=True)
        self.res_4_2 = ResnetBlock(512)
        self.avg_pool = GlobalAveragePooling2D()
        self.flat = Flatten()
        self.fc = Dense(num_classes, activation="softmax")

    def call(self, inputs):
        out = self.reshape(inputs)
        out = self.conv_1(out)
        out = self.init_bn(out)
        out = tf.nn.relu(out)
        out = self.pool_2(out)
        for res_block in [self.res_1_1, self.res_1_2, self.res_2_1, self.res_2_2, self.res_3_1, self.res_3_2, self.res_4_1, self.res_4_2]:
            out = res_block(out)
        out = self.avg_pool(out)
        out = self.flat(out)
        out = self.fc(out)
        return out
#end resnet18


class CustomImageDataset(Dataset):
    def __init__(self, image_labels, transform=None, target_transform=None, **kwargs):

        super(CustomImageDataset, self).__init__()

        self.imgs_labels = image_labels
        self.labels = image_labels['labels'].unique()
        self.transform = transform
        self.target_transform = target_transform
        self.kwargs = kwargs
    def __len__(self):
        return len(self.imgs_labels)

    def __getitem__(self, idx):
        image = self.imgs_labels.iloc[idx, 0]
        label = self.imgs_labels.iloc[idx, 1]
        filename = self.imgs_labels.iloc[idx, 2]
        if self.transform:
            image = self.transform(image, **self.kwargs)
        if self.target_transform:
            label = self.target_transform(label)
        return image, label, filename

class MyEnsemble(nn.Module):

    def __init__(self, modelA, modelB, modelC, modelD, modelE, modelF, modelG, input):
        super(MyEnsemble, self).__init__()
        self.modelA = modelA
        self.modelB = modelB
        self.modelC = modelC
        self.modelD = modelD
        self.modelE = modelE
        self.modelF = modelF
        self.modelG = modelG
        #self.modelA.fc = nn.Identity()
        #self.modelB.fc = nn.Identity()
        #self.modelC.fc = nn.Identity()
        #self.modelD.fc = nn.Identity()
        #self.modelE.fc = nn.Identity()
        #self.modelF.fc = nn.Identity()
        #self.modelG.fc = nn.Identity()
    
        self.size = input
        self.relu=nn.ReLU()
        self.fc2 = nn.Linear(input,input)
        #self.fc2 = self.modelG.fc #.resize_(input, input) #nn.Linear(input, input)

    def forward(self, x): #, q):
        outx = self.modelE(x) #torch.softmax(self.modelE(x), dim=1) #.resize_(x.size(dim=0), self.size))
        outy = self.modelD(x) #torch.softmax(self.modelD(x), dim=1) #.resize_(x.size(dim=0), self.size))
        outz = self.modelC(x) #torch.softmax(self.modelC(x), dim=1) #.resize_(x.size(dim=0), self.size))
        outA = self.modelA(x) #torch.softmax(self.modelA(x), dim=1)
        outB = self.modelB(x) #torch.softmax(self.modelB(x), dim=1)
        outC = self.modelF(x) #torch.softmax(self.modelF(x), dim=1)
        outD = self.modelG(x) #torch.softmax(self.modelG(x), dim=1)

        # Getting average representation of latent space 
        out = torch.add(outA, outB)/2 
        return out

class MyEnsemble2(nn.Module):

    def __init__(self, modelList, input):
        super(MyEnsemble2, self).__init__()
        self.ensemble = VotingClassifier(estimators=modelList, voting='hard')
        #self.ensemble = []
        #for i in modelList[:2]:
        #    self.ensemble.append(VotingClassifier(estimator=i, n_estimators=10))

    def forward(self, x, y): #, q):
        self.ensemble.fit(X, y)
        out = self.ensemble.predict(x)
        #out1 = self.ensemble[0](x)
        #out2 = self.ensemble[1](x)
        #out = out1 + out2 #torch.add(out1,out2)
        #for clf in self.ensemble[1:]:
        #    tt = torch.add(tt(x), clf(x))
            
        #out = torch.minimum(outC, outD) 
        return out #torch.softmax(out, dim=1) #1)

def format_output(fnames, output_probabilities, ss, savepath='', filename=''):
    # Turning the results into a dataframe
    support_set = CustomImageDataset(ss, transform=resize, size=(255, 255))
    for sublist in output_probabilities:
        #for tensor_i in sublist:
        print(len(sublist))
    results = pd.DataFrame(tensor_i.tolist() for sublist in output_probabilities for tensor_i in sublist)

    # Assigning label names to the columns
    results.columns = support_set.labels

    # Formatting filenames for each chip
    flat_list = [os.path.basename(item).split('.')[0] for sublist in fnames for item in sublist]

    # Assigning the index row, column position in the convention of R{row_idx}C{col_idx} to the dataframe.
    results.index = flat_list

    # Creating the prediction column by comparing the max value between the labels.
    results['prediction'] = results.idxmax(axis=1)
   
    # renaming the first column 'chip'
    #self.results.reset_index(inplace=True)
    #self.results = self.results.rename(columns={'index': 'chip'})
    results['chip'] = results.index
    print(results)
    if savepath:
        filepath = os.path.join(savepath, filename)
        results.to_csv(filepath)
    return results

def extract_features(img, model):
    # reshape the data for the model reshape(num_of_samples, dim 1, dim 2, channels)
    reshaped_img = img.reshape(1,64,64,3) 
    # prepare image for model
    imgx = preprocess_input(reshaped_img)
    # get the feature vector
    features = model.predict(imgx, use_multiprocessing=True)
    return features

def get_conf_mat(model_ID, ground_truth, dd):
    #dd = {"STO": "particle", "PtC": "set_3_label", "GE": "set_2_label"}
    #dd = {"grain": "particle" , "void": "set_2_label"}
    f = open("testing/results_"+model_ID+".csv", "r")
    data = f.read()
    f.close()

    data = data.split("\n")
    data = data[:-1]
    t = []
    for i in data:
        temp = i.split(",")
        t.append(temp)
    data = np.array(t)
    print(data)
    pred_dict = {}
    for i in data[1:]:
        if i[-1] not in pred_dict.keys():
            pred_dict[i[-1]] = []
        pred_dict[i[-1]].append(i[-2])

    f = open(ground_truth, "r")
    data_truth = f.read()
    f.close()

    data_truth = data_truth.split("\n")
    data_truth = data_truth[:-1]
    t = []
    for i in data_truth:
        temp = i.split(",")
        t.append(temp)
    data_truth = np.array(t)
    print(data_truth)
    pred_truth = {}
    for i in data_truth[1:]:
        if i[1] in dd.keys():
            if i[0] not in pred_truth.keys():
                pred_truth[i[0]] = []
            pred_truth[i[0]].append(dd[i[1]])

    kk = []
    for x in dd.keys():
        kk.append(dd[x])

    keys = pred_truth.keys()
    pr = []
    tr = []
    for i in keys:
        pr.append(pred_dict[i])
        tr.append(pred_truth[i])
    mm = confusion_matrix(tr, pr, labels=kk) #["particle", "set_3_label", "set_2_label"])
    print(mm)

def get_acc(model_ID, res_file, count, ground_truth, ss, dd):
    #dd = {"STO": "particle", "PtC": "set_3_label", "GE": "set_2_label"}
    #dd = {"grain": "particle" , "void": "set_2_label"}
    f = open("testing/results_"+model_ID+".csv", "r")
    data = f.read()
    f.close()

    data = data.split("\n")
    data = data[:-1]
    t = []
    for i in data:
        temp = i.split(",")
        t.append(temp)
    data = np.array(t)
    print(data)
    pred_dict = {}
    for i in data[1:]:
        if i[-1] not in pred_dict.keys():
            pred_dict[i[-1]] = []    
        pred_dict[i[-1]].append(i[-2])

    #support_dict = {'particle': ['R3C0', 'R19C4', 'R31C0'],
    #            'set_2_label': ['R0C10', 'R10C10', 'R18C12'],
    #            'set_3_label': ['R0C21', 'R5C30', 'R29C29']}
    #support_dict = {'particle': ['R29C0', 'R21C21', 'R32C17'],
    #            'set_2_label': ['R20C7', 'R15C10', 'R10C20'],
    #            'set_3_label': ['R0C12', 'R1C9']}

    #support_dict = {'set_3_label': ['R10C49', 'R10C31', 'R10C34', 'R10C37', 'R10C46'],
    #    'particle': ['R20C20', 'R20C23', 'R30C31', 'R30C33', 'R30C8'],
    #    'set_2_label': ['R40C25', 'R40C27', 'R40C24', 'R40C28', 'R40C31']}    

    f = open(ground_truth, "r")
    data_truth = f.read()
    f.close()

    data_truth = data_truth.split("\n")
    data_truth = data_truth[:-1]
    t = []
    for i in data_truth:
        temp = i.split(",")
        t.append(temp)
    data_truth = np.array(t)
    print(data_truth)
    pred_truth = {}
    for i in data_truth[1:]:
        if i[1] in dd.keys():
            if i[0] not in pred_truth.keys():
                pred_truth[i[0]] = []
            pred_truth[i[0]].append(dd[i[1]])

    #support_dict = ss 
    #print(support_dict)
    #print(pred_truth)
    #keys_ = list(support_dict.keys())
    #support_dict[keys_[0]] = pred_truth[support_dict[keys_[0]][0]]
    #support_dict[keys_[1]] = pred_truth[support_dict[keys_[1]][0]]
    #support_dict[keys_[2]] = pred_truth[support_dict[keys_[2]][0]]

    #keys_2 = list(pred_dict.keys())
    #for i in keys_2:
    #    pred_dict[i][0] = support_dict[pred_dict[i][0]][0] 

    found = 0
    total = 0
    for i in pred_truth.keys():
        if i in pred_dict.keys() and i in pred_truth.keys():
            if pred_dict[i][0] in pred_truth[i][0]:
                found += 1
            total += 1
    if os.path.isfile(res_file) and count == 0:
        os.remove(res_file)
    f = open(res_file, "a") #"accuracy_winnow/acc_res.csv", "a")
    f.write(model_ID +","+str(found/total) + "\n" )
    f.close()

def get_feat(ground_truth):
    f = open(ground_truth, "r")
    data_truth = f.read()
    f.close()

    data_truth = data_truth.split("\n")
    data_truth = data_truth[:-1]
    t = []
    for i in data_truth:
        temp = i.split(",")
        t.append(temp)
    data_truth = np.array(t)
    print(data_truth)
    featureID = []
    for i in data_truth[1:]:
        featureID.append(i[0])
    return featureID    

def get_val(ground_truth):
    f = open(ground_truth, "r")
    data_truth = f.read()
    f.close()

    data_truth = data_truth.split("\n")
    data_truth = data_truth[:-1]
    t = []
    for i in data_truth:
        temp = i.split(",")
        t.append(temp)
    data_truth = np.array(t)
    print(data_truth)
    featureval = []
    for i in data_truth[1:]:
        featureval.append(i[1])
    return featureval 

def get_best_model(res_file):
    f = open(res_file, "r")
    data = f.read()
    f.close()
    data = data.split("\n")
    data = data[:-1]
    t = {}
    acc_val = []
    for i in data:
        temp = i.split(",")
        print(temp)
        t[float(temp[1])] = temp[0]
        acc_val.append(float(temp[1]))
    print(t[max(acc_val)])
    print(max(acc_val))


def get_test_data(ss, qs, l):
    support_set = CustomImageDataset(ss, transform=resize, size=(255, 255))
    stacked_support_set = {}

    # get class inds
    for label in support_set.labels:
        stacked_support_set[label] = torch.stack([x[0] for i, x in enumerate(support_set) if x[1] == label])

    query_set = CustomImageDataset(qs, transform=resize, size=(255, 255))
    query_loader = DataLoader(query_set, batch_size=100)

    return query_loader, stacked_support_set

def get_scalar(lll, ss, qs, enknum, feat, val, dd, query=None):
    #scalar product between chips
    #--> match scalar to label
    #--> define the support set for the existing labels
    #--> use image net to get the output of image chip in base 
    #--> take the average of the three prediction
    #--> take the scalar product of each of the rest of the image with the three axis
    #--> what is the distribution of the scalar product of chips that are labeled at A with the axis A
    #--> look at Jan's notbook
    # Predicting the labels for chips and timing the prediction.

    #Plot a histogram of the euclidean distance for each encoder
    #--> Do that for all chips
    #Get a plot with a distance between axis
    #Y axis is classifier
    #put the hitogram of the average distance for label A va A then Label A vs B and A vs C

    start_time = time.time()
    torch.manual_seed(75)
    norm = Softmax(dim=1)
    output_probabilities = []

    # load the support set data
    support_set = CustomImageDataset(ss, transform=resize, size=(255, 255))
    stacked_support_set = {}

    # get class inds
    for label in support_set.labels:
        stacked_support_set[label] = torch.stack([x[0] for i, x in enumerate(support_set) if x[1] == label])

    query_set = CustomImageDataset(qs, transform=resize, size=(255, 255))

    #feat = get_feat("data/STO_GE_2/STO_GE_2_truth_df.csv")
    #val = get_val("data/STO_GE_2/STO_GE_2_truth_df.csv")
    #data/BiTE_1/BiTE_1_truth_df.csv
    #feat = get_feat("data/BiTE_1/BiTE_1_truth_df.csv")
    #val = get_val("data/BiTE_1/BiTE_1_truth_df.csv")

    #dd = {"STO": "particle", "PtC": "set_3_label", "GE": "set_2_label"}
    #dd = {"grain": "particle" , "void": "set_2_label"}

    l = {}
    for x in range(len(val)):
        if feat[x] not in l.keys():
            l[feat[x]] = [dd[val[x]]]
        else:
            l[feat[x]].append(dd[val[x]])
    
    query_loader = {}
    for i, x in enumerate(query_set):
        if x[2] in l.keys():
            if l[x[2]][0] not in query_loader.keys():
                query_loader[l[x[2]][0]] = []
            query_loader[l[x[2]][0]].append(x[0])

    for label in query_loader.keys():
        t = query_loader[label][::5]
        query_loader[label] = torch.stack(t) #[::10]) #50])
    
    #query_loader = DataLoader(query_set, batch_size=100)
    fnames = []
    prototypes = []
    other_prototypes = []
    distances = []
    inter_axis_distances = {}
    count = 0
    modelList = list(lll)
    # encode the data points
    for s_k, x_k in stacked_support_set.items():
        print(s_k)
        # calculate the prototype for each class
        if query == None:
            temp = []
            for xx in range(len(modelList)):
                temp.append(modelList[xx](x_k))
            if False: #len(modelList) > 1:
                temp1 = []
                for xx in range(len(temp)):
                    temp1.append(temp[xx].mean(0))
                temp2 = torch.stack(temp1)
                prototypes.append(temp2)
            else:
                for i in range(len(temp)):
                    prototypes.append(temp[i])

    lab = []
    for s_k, x_k in query_loader.items():
        print(s_k)
        lab.append(s_k)
        if query == None:
            temp = []
            for xx in range(len(modelList)):
                temp.append(modelList[xx](x_k))
            if False: #len(modelList) > 1:
                temp1 = []
                for xx in range(len(temp)):
                    temp1.append(temp[xx].mean(0))
                temp2 = torch.stack(temp1)
                other_prototypes.append(temp2)
            else:
                for i in range(len(temp)):
                    other_prototypes.append(temp[i])
            #temp2 = torch.stack(temp)
            #other_prototypes.append(temp2.mean(0)) #.mean(0))

    #prototypes = torch.stack(prototypes)
    #other_prototypes = torch.stack(other_prototypes)
    #other_prototypes = other_prototypes.T
 
    for i in range(0,len(other_prototypes), len(modelList)):
        norm = Softmax(dim=1)
        for x in range(len(modelList)):
            distances.append(torch.cdist(other_prototypes[i+x], prototypes[i+x]).mean(1).detach().numpy())
        for j in range(i+len(modelList), len(other_prototypes), len(modelList)):
            for x in range(len(modelList)):
                if lab[int(i/len(modelList))]+"_vs_"+lab[int(j/len(modelList))] not in inter_axis_distances.keys():
                    inter_axis_distances[lab[int(i/len(modelList))]+"_vs_"+lab[int(j/len(modelList))]] = []
                inter_axis_distances[lab[int(i/len(modelList))]+"_vs_"+lab[int(j/len(modelList))]].append(torch.cdist(other_prototypes[j+x], prototypes[i+x]).mean(1).detach().numpy())

    if len(modelList) > 1:
        print("for the model with "+str(len(modelList)) + " models")
        cc = 0
        dd = []
        for i in inter_axis_distances.keys():
            #print(inter_axis_distances[i])
            t = np.mean(inter_axis_distances[i], axis=0) #torch.stack(inter_axis_distances[i])
            inter_axis_distances[i] = [t] #.mean(0)
            #print(len(inter_axis_distances[i]))
            t1 = np.mean(distances[cc*len(modelList):(cc+1)*len(modelList)], axis=0)
            #print(t1.shape)
            if len(t1.shape) > 0:
                dd.append(t1)
            cc += 1
        distances = dd

    print("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH")
    print(np.array(distances).shape)
    print(len(lab))
    print(lab)

    for x in range(len(distances)):
        tt = ""
        if os.path.isfile("mean-dist/"+lab[x]+".txt") and enknum == 0:
            os.remove("mean-dist/"+lab[x]+".txt")
        for s in distances[x]:
            tt += str(s)+","
        tt = tt[:-1] + "\n"
        f = open("mean-dist/"+lab[x]+".txt", "a")
        f.write(tt)
        f.close()

    for x in inter_axis_distances.keys():
        tt = ""
        if os.path.isfile("mean-dist/"+x+".txt") and enknum == 0:
            os.remove("mean-dist/"+x+".txt")
        for s in inter_axis_distances[x][0]:
            tt += str(s)+","
        tt = tt[:-1] + "\n"
        f = open("mean-dist/"+x+".txt", "a")
        f.write(tt)
        f.close()

def create_heatmap(filename):
    list_files = glob.glob(filename+"/*.txt")
    print(list_files)
    dd = {}
    label = ["particle", "set_2_label"] #["set_3_label", "particle", "set_2_label"]
    delimiter = "_vs_"
    for x in list_files:
        f = open(x, "r")
        data = f.read()
        f.close()
        data = data.split("\n")
        data = data[:-1]
        t = []
        for i in data:
            temp = i.split(",")
            tt = []
            for w in temp:
                tt.append(float(w))
            t.append(tt)
        data = np.array(t)
        name_file = x.split("/")
        name_file = name_file[-1].split(".")
        nf = name_file[0]
        name_file = name_file[0] + "_heatmap.png"
        dd[nf] = data

        plt.figure(figsize=(10, 10))
        ax = sns.heatmap( data , linewidth = 0.5 , cmap = 'coolwarm' )
        plt.title( "Heat-map of euclidean distance vs model" )
        plt.tight_layout()
        plt.savefig(filename+"/"+name_file)

    ddV2 = {}
    lab = []
    for x in dd.keys():
        lab.append(x)
        if delimiter in x:
            temp = x.split(delimiter)
            for y in range(len(dd[x])):
                if temp[0] not in ddV2.keys():
                    ddV2[temp[0]] = {}
                if temp[1] not in ddV2.keys():
                    ddV2[temp[1]] = {}
                if y not in ddV2[temp[0]].keys():
                    ddV2[temp[0]][y] = {}
                if y not in ddV2[temp[1]].keys():
                    ddV2[temp[1]][y] = {}
                ddV2[temp[0]][y][temp[1]] = dd[x][y]
                ddV2[temp[1]][y][temp[0]] = dd[x][y]
        else:
            for y in range(len(dd[x])):
                if x not in ddV2.keys():
                    ddV2[x] = {}
                if y not in ddV2[x].keys():
                    ddV2[x][y] = {} 
                ddV2[x][y][x] = dd[x][y] 

    model_ID = ["ensemble", "federated" ,"resnet101","resnet18","resnet34","resnet50","resnet152","torch101","shufflenet"]

    for cue in ddV2.keys():
        kk = list(ddV2[cue])
        fig = plt.figure(figsize=(10, 10))
        gs = fig.add_gridspec(len(kk), hspace=0)
        axs = gs.subplots(sharex=True, sharey=True)
        fig.suptitle('Distance between support\nand none support set chips')
        count = 0
        for x in ddV2[cue].keys():
            key_arr = sorted(list(ddV2[cue][x].keys()))
            for y in key_arr: #ddV2[cue][x].keys():
                tt = {}
                LL = []
                for dist in ddV2[cue][x][y]:
                    LL.append(round(dist))
                    if int(round(dist)) not in tt.keys():
                        tt[int(round(dist))] = 0
                    tt[int(round(dist))] += 1
                print("This is the number of items that have a specific distance "+ cue + " " +y)
                XX = list(tt.keys())
                YY = list(tt.values())
                print(XX)
                print(YY)
                print(LL)
                num_bins = len(XX)
                axs[count].hist(LL, num_bins,label=y, histtype='step') #bar(XX,YY,label=y)
                axs[count].set_ylabel(model_ID[count])
                #axs[count].plot(ddV2[cue][x][y], label=y)
            count += 1
        plt.legend()
        plt.tight_layout()
        plt.savefig(filename+"/"+cue+"-histogram.png")

    # TODO:
    # fix the colors to be the same
    # connect to RC support (Deception) --> check VSCode, use the remote ssh extension and open the file on deception
    # lauch jupiter notebook on Deception
    # potential: select random set of chip
    # change to step histogram
    # compute the distance between the histogrom with KS distance or earth movers distance
    # How much do you have to move from one histogram to another to make it look like the other
    # add ensemble learning result to the histogram
    # some networks might be better at identifying some classes vs others
    # --> get the best of both worlds by combining them
    # cross between features and models
    # of the pairs which one should I pay attention too?

    # It doesnt realistically imply the power system --> we use DNP3 --> realism!!!
    # OSI layer most of the develloper are stuck on the operation layer
    # We takle every OSI layer
    # Our co-simulation tackles the entire stack --> simulate very close to real test bed
    # Future integrate modbus
    # RTS test bed --> send command to relay
    # On off the commad
    # We focuse on the effect on the network
    # Start the attack on the communication layer
    # The reconfiguration network --> slices... 3 different characteristic. 
    # The datasets are a main useful tool


    #How unique are datasets based on device types
    #--> quantifying the difference between the datasets

    #Code for calculating the distance of the rest of the chips
    #model.eval()
    #with torch.no_grad():
    #    for i, q in enumerate(query_loader):
    #        # q[0] is a stack of image data q[1] can be ignored
    #        print("Computing batch for scalar %s" % str(i))
    #        output = model(stacked_support_set, q[0])
    #        probabilities = norm(output)
    #        output_probabilities.append(probabilities)
    #        fnames.append(q[2])

    # Turning the results into a dataframe
    #results = pd.DataFrame(tensor_i.tolist() for sublist in output_probabilities for tensor_i in sublist)

    # Assigning label names to the columns
    #results.columns = support_set.labels

    # Formatting filenames for each chip
    #flat_list = [os.path.basename(item).split('.')[0] for sublist in fnames for item in sublist]

    # Assigning the index row, column position in the convention of R{row_idx}C{col_idx} to the dataframe.
    #results.index = flat_list

    # Creating the prediction column by comparing the max value between the labels.
    #results['prediction'] = results.idxmax(axis=1)

    #print(results)

def sort_feat(modelID, qs, ss):
    test_image = ss["images"]
    image_labels = ss["labels"]
    chip_names = ss["filename"]

    #model = ResNet18(1)#the number is the number of output classes in the classification --> our case that is 1
    #model.build(input_shape = (None,x.shape[1])) #*3))
    model2 = VGG19(include_top=False, input_shape=(64,64,3))
    model2 = Model(inputs = model2.inputs, outputs = model2.output) #[-2]
    #model = ResNet50(include_top=False, input_shape=(64,64,3),weights='imagenet')
    #model2 = Model(inputs = model.input,outputs = model.output)
    print(image_labels)
    image_dict = {}
    for i in test_image.keys():
        t = resize(test_image[i],size=(64, 64)).numpy()
        image_dict[i] = extract_features(t,model2)  
    filenames = np.array(list(image_dict.keys())) 
    feat = np.array(list(image_dict.values()))
    feat = feat.reshape(1296,2048) #8192) #12800)
    #remove PCA
    pca = PCA(n_components=400, random_state=22)
    pca.fit(feat)
    x = pca.transform(feat)
    kmeans = KMeans(n_clusters=3, random_state=22)
    kmeans.fit(x)
    groups = {}
    group_chips = {}
    for file, cluster in zip(filenames,kmeans.labels_):
        if cluster not in groups.keys():
            groups[cluster] = []
        if cluster not in group_chips.keys():
            group_chips[cluster] = []
        groups[cluster].append(file)
        group_chips[cluster].append(test_image[file].numpy())

    X = []
    Y = []
    label_to_num = {}
    count = 0
    for y in test_image.keys():
        X.append(resize(test_image[y],size=(255, 255)).numpy().reshape((255,255,3))) #83*83*3,))
        if image_labels[y] not in label_to_num.keys():
            label_to_num[image_labels[y]] = count
            count += 1
        Y.append(label_to_num[image_labels[y]])
    
    X = x #np.array(X)
    Y = np.array(Y)
    X_new = X.reshape((X.shape[0], X.shape[1]))#*X.shape[3]))
    print(X.shape)
    print(Y.shape)
   
    print("start of the layer collection")
    score2 = X_new 
    
    t = []
    for i in score2:#[0]:
        tt = i.reshape((20,20))
        t.append(tt)
    t = np.array(t)

    av_weight = []
    weights_feature = {}
    inv_weights_feat = {}
    count = 0
    for i in score2: #[0]:
        weights_feature[np.std(i)] = chip_names[count]
        inv_weights_feat[chip_names[count]] = np.std(i) #+sum(i)/len(i)
        av_weight.append(np.std(i)) #+sum(i)/len(i))
        count += 1
    print("This is the average weights per images")
    av_weight = np.array(sorted(av_weight))
    print(av_weight)
    print(weights_feature)

    for i in range(4): #len(t)):
        plt.figure(figsize=(10, 10))
        chip = qs[chip_names[i]]
        ax = sns.heatmap( t[i] , linewidth = 0.5 , cmap = 'coolwarm' )
        plt.title( "Heat-map of pixels used chips" )
        plt.tight_layout()
        plt.savefig("feature_heatmap_chip"+str(i)+".png")

        fig, axs = plt.subplots(figsize=(10, 10))
        axs.imshow(chip)
        axs.axis('off')
        plt.savefig("chip_"+str(i)+".png")

    #cluster
    new_cluster = {}
    for i in groups.keys():
        #Arrange the chips by weights
        t = groups[i]
        weights = []
        for x in t:
            weights.append(inv_weights_feat[chip_names[x]])
        weights = sorted(weights)
        temp = []
        for x in weights:
            if x > max(weights)-(0.1*max(weights)):
                temp.append(weights_feature[x])
        for x in range(len(temp)): #groups[i])):
            if "set_"+str(i)+"_label" not in new_cluster.keys():
                new_cluster["set_"+str(i)+"_label"] = []
            new_cluster["set_"+str(i)+"_label"].append(temp[x]) #chip_names[groups[i][x]]) 
    return new_cluster

