import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})
import numpy as np
import glob
import csv

mean = glob.glob("data/mean_RE*")
std = glob.glob("data/std_RE*")

mean = sorted(mean)
std = sorted(std)


mean_ = []
std_ = []
total_met = []

for i in range(len(mean)):
    f = open(mean[i], "r")
    f1 = open(std[i], "r")
    content = f.read()
    content1 = f1.read()
    content = content.split("\n")
    content1 = content1.split("\n")
    mean_.append(content[:-1])
    std_.append(content1[:-1])
    f.close()
    f1.close()

print(mean)
print(std)
x = [5,3,2,2,9]#[5,28, 6, 2,16,1]
y = ["1 Dense HL", "2 Dense HL", "1 LSTM HL", "1 GRU HL", "1 RNN HL"]
#std_ = np.array(std_)
#mean_ = np.array(mean_)
#print(len(std_[0]))
#print(len(mean_[0]))

fig = plt.figure()
fig.set_figheight(8)
fig.set_figwidth(8)
plt.subplot(1, 1,1)
plt.bar(range(len(x)), x, align='center', alpha=0.5)
plt.xticks(range(len(x)), y, fontsize=14)
plt.ylabel('Number of features selected')
plt.title('Number of features selected by different models')
plt.tight_layout()
fig.savefig("model_comp_res.png")

for i in range(len(mean_)):
    for j in range(len(mean_[i])):
        mean_[i][j] = float(mean_[i][j])
for i in range(len(std_)):
    print(len(std_[i]))
    for j in range(len(std_[i])):
        std_[i][j] = float(std_[i][j])
total_met.append(mean_)
total_met.append(std_)

for i in range(len(total_met)):
    fig = plt.figure()
    fig.set_figheight(10)
    fig.set_figwidth(12)
    iplot = 1
    label = ["Mean", "Standard_Deviation"]
    plt.subplot(1, 1,1)
    #xs = np.arange(len(mean_[0]))
    legend = []
    for j in range(len(total_met[i])):
        p = total_met[i][j]
        xs = np.arange(len(p))
        #plt.scatter(xs, p, lw=1, marker=".")
        plt.scatter(x[j], p[x[j]], lw=1, s=80, marker="o")
        #legend.append("Model " + str(j))
        legend.append("Model "+ str(j))
    plt.title(label[i]+ " of Absolute Relative Error of selected number of features")
    plt.xlabel("Number of elements in subset")
    x_t = np.arange(1, len(xs)+1, 5)
    #plt.xaxis.ticklabels(x)
    plt.ylabel(label[i] )
    plt.xticks(x_t)#xs + 1)
    if iplot==1:
        plt.legend(legend, loc="upper center", ncol=len(x), mode="expand", borderaxespad=0.)
    iplot += 1
    plt.tight_layout()
    fig.savefig(label[i]+".png")
