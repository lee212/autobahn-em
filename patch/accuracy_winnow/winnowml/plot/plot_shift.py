import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 18})
from get_input import input_data
from get_output import output_data
from MLE2 import MLE
import numpy as np
import statistics
import math
from scipy.spatial import distance
from sklearn.decomposition import PCA
import statistics
from scipy.stats import kurtosis
from scipy.stats import skew
from sklearn.manifold import TSNE
import umap
from sklearn.linear_model import Lasso, LogisticRegression
from sklearn.feature_selection import SelectFromModel
from sklearn.decomposition import SparsePCA
from sklearn.decomposition import PCA, IncrementalPCA
from keras import backend as K
from pystreamfs.algorithms import fsds, ofs, efs
from pystreamfs import pystreamfs
from keras.optimizers import RMSprop, Adam, SGD


in_ = input_data()
out_ = output_data()
mle = MLE()
data, feature_list = in_.get_input_features('../list_maker/data/20191019.csv', 350500, ID_=(23,"fuse"))#"eos/draining"))# 150000)#, ID_=(23,"eos/draining")) #sc-extract-cms-2019-05.csv 95000 out.csv ../list_maker/data/20191019.csv
i = 0
target_vals = in_.get_target(data)#, feature_list[i])
target_IDs = ["Throughput"]#feature_list[i]
t_feature_list = list(feature_list.copy())
target_vals = np.array(target_vals)
target_vals = target_vals.transpose()
target_vals = target_vals.reshape((target_vals.shape[0], ))
dict_fsid = {}
list_fsid = data["fsid"]
list_fsid = list_fsid[500:]
for i in range(100):#len(list_fsid)):
    ID = float(list_fsid[i])
    #ID = round(ID/100)
    if ID not in dict_fsid.keys():
        dict_fsid[ID] = []
    dict_fsid[ID].append(target_vals[i])
    for x in dict_fsid.keys():
        if float(x) != ID:
            dict_fsid[float(x)].append(0)

fig, ax = plt.subplots()
ax.plot(list(range(len(target_vals))), target_vals)

ax.set(xlabel='timestep', ylabel='Normalized throughput', title='Normalized throughput over time')
ax.grid()

plt.tight_layout()

fig.savefig("tp_time.png")
