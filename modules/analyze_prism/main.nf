process analyze_h5 {
    label 'short'

    def start1=""
    def end=""
    def runtime=""
    def FILESIZE=""

    input:
    val(rr)
    val(reads)
    val(bucketID) 
    val(temp)
    val(start)
    val(done)

    output:
    val (reads), emit: x

    script:
    """
    #!/bin/bash
    which aws
    aws --version
    if [ -f "${reads}.h5" ]; then
        echo "${reads}.h5 exists"
    else
        start1=`date +%s`
        aws s3 cp s3://nextflow-bucket-s3/data/${reads}.h5 . 
        end=`date +%s`
        runtime=\$((end-start1))
        FILESIZE=\$(stat -c%s "${reads}.h5")
        flock -x 1 
        echo "\$FILESIZE \$runtime" >> /people/belo700/CHESS/nextflow-dir/nextflow-workflow/downloadS3HPC.txt
        flock -u 1
    fi
    pip install py4DSTEM==0.13.8
    pip install numpy==1.23.5
    pip install matplotlib
    pip install yaml
    if [ -f "example.py" ]; then
        rm example.py
    fi
    cat << EOF >> example.py
    import h5py
    import numpy as np
    from PIL import Image
    import py4DSTEM
    import os
    from py4DSTEM.visualize import show    
    import matplotlib.pyplot as plt
    import yaml
 
    py4DSTEM.check_config()
    py4DSTEM.io.print_h5_tree("${reads}.h5")
    # data from the image
    filename = "${reads}.h5"
    bname = os.path.basename(filename)
    plt_bname = f"{bname.split('.')[0]}"
    virtual_detector = py4DSTEM.io.read(filename, data_id="virtual_detector_depth0000")
    inner_angle = 11
    outer_angle = 20
    virtual_detected = virtual_detector.data[:, :, inner_angle:outer_angle]
    sum_slice = np.sum(virtual_detected, axis=-1)
    cmap = "gray"
    max_angle = virtual_detector.data.shape[-1] 
    inner_angles = [0, 11, 20, 61]
    outer_angles = [20, 20, 40, max_angle]
    labels = ["BF", "ABF", "ADF", "HAADF"]
    data_list = []
    for inner, outer in zip(inner_angles, outer_angles):
        im = virtual_detector.data[:, :, inner:outer]
        data_list.append(np.sum(im, axis=-1))

    for i in range(4):
        fig = plt.imshow(data_list[i], cmap='gray', interpolation=None, resample=False)
        plt.axis("off")
        fpath = f"${start}/{plt_bname}_{labels[i]}.tiff"
        plt.savefig(fpath, bbox_inches="tight", pad_inches=0)

    # angular analysis
    angular_detect = py4DSTEM.io.read("${reads}.h5", data_id='annular_detector_depth0000')
    print(angular_detect.data)
    EOF
    mkdir ${start}
    ls 
    python example.py
    echo "----------------------------"
    ls ${start}
    start1=`date +%s%N`
    aws s3 cp ${start}/ s3://nextflow-bucket-s3/${start}/ --recursive
    end=`date +%s%N`
    runtime=\$((end-start1)) 
    FILESIZE=\$(du -sh --apparent-size ${start} | awk '{ print \$1 }')
    flock -x 1
    echo "\$FILESIZE \$runtime" >> /people/belo700/CHESS/nextflow-dir/nextflow-workflow/downloadHPCS3.txt
    flock -u 1
    export done=\$(( \$done + 1 ))
    """
}
