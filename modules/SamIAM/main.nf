process SAMIAM {
    label 'short'
    def done=0
    def comp_name=""    

    input:
    val(st)
    val(ext)
    val(name)

    script:
    """
    #!/bin/bash

    # export PATH=/opt/conda/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:$PATH

    echo "00000000000000000000000000000000"

    export PATH=$PATH:/people/belo700/.local/bin

    module purge
    # module load python/miniconda3.8

    # source /share/apps/python/miniconda3.8/etc/profile.d/conda.sh
    module load python/3.11.5 
    module load cuda/11.1
    module load gcc/9.1.0    
    # pip install --user git+https://github.com/zhanghang1989/PyTorch-Encoding/
    # pip install --user git+https://github.com/facebookresearch/segment-anything.git
    # pip install --user git+https://github.com/huggingface/transformers.git
    # pip install --user datasets
    # pip install --user monai
    # pip install --user patchify
    pip install --user --upgrade torch
    cp ../../../modules/SamIAM/utils.py .
    cp ../../../modules/SamIAM/unsupervised.py .
    cp ../../../*.pth .
    cp -r ../../../patch/models .
    cp -r ../../../patch/FAPool .
    cp -r ../../../patch/data/ .
    python unsupervised.py --ext=${ext} --imid=${name} --model=FENet --embed=0 --post=1
    export done=1
    """
}
