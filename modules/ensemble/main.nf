process chip_sel {
    label 'short'
    def sel_chips=""

    input:
    val(rr)
    val(reads)
    val(feat_sel_ID)
    val(start) 

    output:
    env(sel_chips), emit: chips
    val(reads), emit: ww   
    val(feat_sel_ID), emit: mm
    val(start), emit: ext2

    script:
    """
    #!/bin/bash
    which aws
    aws --version
    
    module purge
    # module load cuda/11.1
    module load gcc/9.1.0

    pip install --user pyyaml
    pip install --user py4DSTEM==0.13.8 matplotlib yaml tensorflow seaborn opencv-python pycocotools matplotlib onnxruntime onnx 
    if [ -f "chip_sel.py" ]; then
        rm chip_sel.py
    fi
    cat << EOF >> chip_sel.py
    import pandas as pd
    import numpy as np
    import os
    import sys
    import matplotlib.pyplot as plt
    import matplotlib.image as mpimg
    from accuracy_winnow.get_accuracy import get_acc, get_best_model, get_feat, get_val, sort_feat, get_scalar, create_heatmap, MyEnsemble, get_test_data, format_output, MyEnsemble2, get_conf_mat
    from few_shot.resnet import resnet18, resnet34, resnet50, resnet101, resnet152
    import torchvision.models as models
    from few_shot.protonet import PrototypicalNet
    import glob
    import shutil
    from few_shot.pychip_classifier import PychipClassifier
    from IPython.display import Image, display, Markdown

    DIRNAME = os.path.dirname(__file__)
    DEFAULT_DIR = os.path.join(DIRNAME, "..", "chips")

    data_path = "./data/" + sys.argv[2]
    img_name = sys.argv[2] + "." + sys.argv[1]
    results_path = "./testing"
    chips_path = "./testing/chips"    

    n_columns = 128 
    n_rows = 96 

    img_path = os.path.join(data_path, img_name)
    img = mpimg.imread(img_path)
    width = img.shape[0]
    chipsize = width/n_columns
    height = img.shape[1]
    chipheight = height/n_rows
    x_grid_lst = np.linspace(chipsize, width-chipsize, n_columns-1)
    y_grid_lst = np.linspace(chipsize, height-chipsize, n_rows-1)   
 
    from PIL import Image
    import math
    pil_image = Image.open(img_path) 
    img_og = np.asarray(pil_image)
    img = np.asarray(pil_image)
    img_og = img_og[:,:,0]
    img_og = img_og.reshape((img_og.shape[0], img_og.shape[1]))
    print(img_og.shape)
    x = np.unique(img_og)
    group = 3
    ind = round((len(x)/group))
    dd = {}
    for i in range(group):
        dd[i] = x[i*ind:(i+1)*ind]

    num_cols = 128  
    num_rows = 96  
    img_shape = img_og.shape
    height = img_shape[0]
    width = img_shape[1]
    chip_size = round(math.floor(width / num_cols)) 
    print(chip_size)
    print(width)
    num_chips_x = math.floor(width / chip_size) 
    num_chips_y = math.floor(height / chip_size) 
    pixels_ignored_in_x = width % chip_size
    pixels_ignored_in_y = height % math.floor(height / num_rows) 
    x_coords = list(range(0, width, chip_size))
    y_coords = list(range(0, height, math.floor(height / num_rows))) 
    grid_points = []

    for col_idx, x_coord in enumerate(range(0, width - pixels_ignored_in_x, chip_size)):
        for row_idx, y_coord in enumerate(range(0, height - pixels_ignored_in_y, chip_size)):
            grid_points.append((x_coord, y_coord, row_idx, col_idx))

    img = img[0:height - pixels_ignored_in_y, 0:width - pixels_ignored_in_x]

    chips = {}

    for x, y, R_idx, C_idx in grid_points:
        name = f"R{R_idx}C{C_idx}"  
        chips[name] = img[y:y + chip_size, x:x + chip_size]

    feat = get_feat("data/"+sys.argv[2]+"/"+sys.argv[2]+"_truth_df.csv")
    val = get_val("data/"+sys.argv[2]+"/"+sys.argv[2]+"_truth_df.csv")
    t = {}
    for i in range(len(feat)):
        t[feat[i]] = val[i]
    tt = {}
    for i in t.keys():
        if t[i] not in tt.keys():
            tt[t[i]] = []
        tt[t[i]].append(chips[i])

    import torch
    import os
    import math
    from PIL import Image
    from torch.nn import Softmax
    import torch.optim as optim
    import cv2
    import torchvision.transforms as transforms

    def cv2_to_PIL(cv2_img):
        PIL_img = transforms.ToTensor()(Image.fromarray(255 * cv2_img.astype(np.uint8)))
        return PIL_img

    count = 0
    n_columns = num_cols  
    n_rows = num_rows  
    sive = 2048

    ff = sys.argv[3] 
    ff = ff.split(",")
    dd = {}
    count = 0
    for i in ff:
        dd[i] = "set_"+str(count)+"_label"
        count += 1

    new_feat = []
    new_val = []
    for x in range(len(feat)):
        new_feat.append(feat[x])
        if val[x] in dd.keys():
            new_val.append(val[x])
    feat = new_feat
    val = new_val
    l = {}
    for x in range(len(val)):
        if feat[x] not in l.keys(): 
            l[feat[x]] = [dd[val[x]]]
        else:
            l[feat[x]].append(dd[val[x]])

    ss = {}
    for x in range(len(val)):
        if dd[val[x]] not in ss.keys():
            ss[dd[val[x]]] = [feat[x]]
        else:
            ss[dd[val[x]]].append(feat[x])

    path_to_img = os.path.join(data_path, img_name)
    pil_image = Image.open(path_to_img)

    img_og = np.asarray(pil_image)
    if len(img_og.shape) == 2:
        for_torch = np.zeros((img_og.shape[0], img_og.shape[1], 3))
        for_torch[:, :, 0] = img_og  
        for_torch[:, :, 1] = img_og
        for_torch[:, :, 2] = img_og
        img = for_torch
    else:
        img = np.asarray(pil_image)
    
    clip_limit = 1.0
    tile_grid_size = (8, 8)
    clahe = cv2.createCLAHE(clip_limit, tile_grid_size)
    if not isinstance(img.flat[0], np.uint8):
        print('cv2 requires different datatype, converting to uint8')
        img = img.astype(np.uint8)
    
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    hsv_planes = list(cv2.split(hsv_img))
    hsv_planes[2] = clahe.apply(hsv_planes[2])
    hsv = cv2.merge(hsv_planes)
    img = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

    img_shape = img_og.shape
    height = img_shape[0]
    width = img_shape[1]
    chip_size = math.floor(width / n_columns)
    num_chips_x = math.floor(width / chip_size) 
    num_chips_y = math.floor(height / chip_size) 
    pixels_ignored_in_x = width % chip_size
    pixels_ignored_in_y = height % math.floor(height / n_rows) 
    x_coords = list(range(0, width, chip_size))
    y_coords = list(range(0, height, math.floor(height / n_rows))) 
    grid_points = []

    for col_idx, x_coord in enumerate(range(0, width - pixels_ignored_in_x, chip_size)):
        for row_idx, y_coord in enumerate(range(0, height - pixels_ignored_in_y, chip_size)):
            grid_points.append((x_coord, y_coord, row_idx, col_idx))

    img = img[0:height - pixels_ignored_in_y, 0:width - pixels_ignored_in_x]

    chips = {}
    for x, y, R_idx, C_idx in grid_points:
        name = f"R{R_idx}C{C_idx}"  
        chips[name] = img[y:y + chip_size, x:x + chip_size]

    img_chips = []
    for chip_name in chips:
        image = cv2_to_PIL(chips[chip_name])
        if chip_name in l.keys():
            img_chips.append([image, l[chip_name][0], chip_name])

    corr = {}
    eucl_dist = {}
    img_chips2 = img_chips[::50]
    keys = []
    for i in range(len(img_chips2)):
        ii1 = img_chips2[i][0].mean(0).detach().numpy()
        ii = ii1.reshape((ii1.shape[0]*ii1.shape[1],))
        name = img_chips2[i][2]
        keys.append(name)
        corr[name] = []
        eucl_dist[name] = []
        for j in range(len(img_chips2)):
            ii3 = img_chips2[j][0].mean(0).detach().numpy()
            ii2 = ii3.reshape((ii3.shape[0]*ii3.shape[1],))
            corr[name].append(np.corrcoef(ii,ii2)[0,1])
            eucl_dist[name].append(np.linalg.norm(ii-ii2))#-torch.cdist(img_chips[i][0], img_chips[j][0]))
    print(eucl_dist)

    from scipy.spatial import distance
    eucl_dist = {}
    keys = []
    for i in range(0,len(img_chips),4):
        ii = img_chips[i][0].mean(0).detach().numpy()
        name = img_chips[i][2]
        keys.append(name)
        eucl_dist[name] = []
        for j in range(0,len(img_chips),4):
            if img_chips[j][1] in img_chips[i][1]:
                ii2 = img_chips[j][0].mean(0).detach().numpy()
                eucl_dist[name].append(-distance.cdist(ii, ii2, 'euclidean'))
    reward = eucl_dist
    
    rew = []
    for i in keys:
        rew.append(reward[i])
    rew = np.array(rew)
    high_rew = {}
    for i in range(len(rew)):
        high_rew[keys[i]] = []
        for j in range(len(rew[i])):
            high_rew[keys[i]].append(keys[j])
    num_rel = {}
    for i in keys:
        if len(high_rew[i]) not in num_rel:
            num_rel[len(high_rew[i])] = []
        num_rel[len(high_rew[i])].append(i)
    sorted_keys = sorted(list(num_rel.keys()))
    sorted_keys = sorted_keys[::-1]

    new_feat = []
    new_val = []
    for x in range(len(feat)):
        new_feat.append(feat[x])
        if val[x] in dd.keys():
            new_val.append(val[x])
    feat = new_feat
    val = new_val

    l = {}
    for x in range(len(val)):
        if feat[x] not in l.keys():
            l[feat[x]] = [val[x]]
        else:
            l[feat[x]].append(val[x])
    sel_feat = {}
    for x in range(len(sorted_keys)):
        for i in num_rel[sorted_keys[x]]:
            if l[i][0] not in sel_feat.keys():
                sel_feat[l[i][0]] = []
            sel_feat[l[i][0]].append(i)
        
    support_dict = {}
    ff = os.path.join(DEFAULT_DIR, "chips.txt")
    f = open(ff, "a")
    for i in sel_feat.keys():
        support_dict[dd[i]] = sel_feat[i][:50]
        print(i)
        rr = i+":"
        for x in sel_feat[i][:50]:
            rr += x + ","
        rr = rr[:-1] + "|"
        f.write(rr)
        print(rr)
    f.close()
    EOF

    git clone ssh://git@gitlab-data.pnnl.gov:2222/joecool/pychip-classifier.git
    mkdir chips
    mv chip_sel.py pychip-classifier/
    cd pychip-classifier
    cp -r ../../../../patch/accuracy_winnow/ .
    cp -r ../../../../patch/data/ .
    python chip_sel.py "${start}" "${reads}" "${feat_sel_ID}"
    cd ../
    export sel_chips=`cat chips/chips.txt`
    echo \${sel_chips}
    """
}

process ensemble_model {
    label 'short'

    def xx=""
    def out=""

    input:
    val(reads)
    val(molID)
    val(feat_sel_ID)
    val(round)
    val(ext)

    output:
    env(out), emit: i

    script:
    """
    #!/bin/bash
    which aws
    aws --version
    mkdir img
    cd img
    echo "${molID}*"
    aws s3 cp s3://nextflow-bucket-s3/${molID}/ . --recursive --include "${molID}*"
    export xx=` ls * `
    echo $xx
    cd ..
    pip install --user py4DSTEM==0.13.8
    pip install --user matplotlib
    pip install --user yaml
    pip install --user tensorflow
    pip install --user seaborn
    if [ -f "model.py" ]; then
        rm model.py
    fi
    cat << EOF >> model.py
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from accuracy_winnow.get_accuracy import get_acc, get_best_model, get_feat, get_val, sort_feat, get_scalar, create_heatmap, MyEnsemble, get_test_data, format_output, MyEnsemble2, get_conf_mat
from few_shot.resnet import resnet18, resnet34, resnet50, resnet101, resnet152
import torchvision.models as models
from few_shot.protonet import PrototypicalNet
import glob
import shutil
from few_shot.pychip_classifier import PychipClassifier
from IPython.display import Image, display, Markdown
import sys

def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False

input_chips = "${reads}" 
input_chips = input_chips.split("|")
support_dict = {}
for x in input_chips[:-1]:
    temp = x.split(":")
    support_dict[temp[0]] = temp[1].split(",")
print(support_dict)

data_path = "./data/" + sys.argv[1]
img_name = sys.argv[1] + "." + sys.argv[2]
results_path = "./testing"
chips_path = "./testing/chips"


import torch
from torch.nn import Softmax
import torch.optim as optim
import math
count = 0
n_columns = 128 #64 #128 #50 #64 #50 #128 
n_rows = 96 #48 #96 #50 #48 #50 #96 
sive = 2048

feat = get_feat("data/"+sys.argv[1]+"/"+sys.argv[1]+"_truth_df.csv")
val = get_val("data/"+sys.argv[1]+"/"+sys.argv[1]+"_truth_df.csv")

ff = sys.argv[3] 
ff = ff.split(",")
dd = {}
count = 0
for i in ff:
    dd[i] = "set_"+str(count)+"_label"
    count += 1

new_feat = []
new_val = []
for x in range(len(feat)):
    new_feat.append(feat[x])
    if val[x] in dd.keys():
        new_val.append(val[x])
val = new_val
feat = new_feat

l2 = {}
for x in range(len(val)):
    if feat[x] not in l2.keys():
        l2[feat[x]] = [dd[val[x]]]
    else:
        l2[feat[x]].append(dd[val[x]])

l = {}
for x in range(len(val)):
    if feat[x] not in l.keys():
        l[feat[x]] = [val[x]]
    else:
        l[feat[x]].append(val[x])

ss = {}        
for i in support_dict.keys():
    ss[dd[i]] = support_dict[i][:30]
support_dict = ss
 
classifier = PychipClassifier(data_path, img_name)
classifier.preprocess("CLAHE", clipLimit=2.0)
classifier.chips_genesis(n_rows, n_columns)
print(support_dict)
classifier.support_genesis(support_dict=support_dict)
  
res18 = resnet18(pretrained=True, place_on_device=False) 
res34 = resnet34(pretrained=True, place_on_device=False) 
res50 = resnet50(pretrained=True, place_on_device=False) 
res101 = resnet101(pretrained=True, place_on_device=False) 
res152 = resnet152(pretrained=True, place_on_device=False) 
res101V2 = models.resnet101(pretrained=True) 
shuffle = models.shufflenet_v2_x1_0(pretrained=True)

Ensemble = [res18,res34, res50, res101, res152] #, res101V2, shuffle]
encoder_list = [ "resnet18", "resnet34", "resnet50", "resnet101", "resnet152"] #, "torch101", "shufflenet"]

full_model = {}
fname = []
for i in range(len(encoder_list)):
    model = PrototypicalNet(encoder=Ensemble[i], device='cpu')
    classifier.predict(savepath=results_path, encoder=encoder_list[i], seed=75, filename='results_tt_'+encoder_list[i]+'.csv') 
    get_acc("tt_"+encoder_list[i], "accuracy_winnow/acc_temp_res.csv", i, "data/"+sys.argv[1]+"/"+sys.argv[1]+"_truth_df.csv", support_dict, dd)
    temp = classifier.results.to_numpy()
    if i == 0:
        fname = classifier.results["chip"].to_numpy()
    tt = temp[:,:len(list(support_dict.keys()))]
    for j in range(tt.shape[1]):
        if j not in full_model.keys():
            full_model[j] = []
        full_model[j].append(tt[:,j])
        
f = open("accuracy_winnow/acc_temp_res.csv", "r")
data = f.readlines()
f.close()
t = {}
acc_val = []
for i in data:
    temp = i.split(",")
    print(temp)
    t[float(temp[1])] = temp[0]
    acc_val.append(float(temp[1]))
print(acc_val)

print(full_model)
    
for i in full_model.keys():
    t = np.array(full_model[i])
    temp = []
    print(t.shape)
    for x in range(t.shape[1]):
        sum_weighted = 0
        count = 0
        for j in t[:,x]:
            if isfloat(j):
                sum_weighted += math.exp(acc_val[count]*100)*j
                count = count +1
        temp.append(sum_weighted/len(t[:,x]))
    full_model[i] = np.array(temp)

total = []
for i in full_model.keys():
    total.append(full_model[i])
total = np.array(total).T

print(total)
print(total.shape)
print(list(support_dict.keys()))

final_df = pd.DataFrame(total, columns = list(support_dict.keys())) #["set_3_label", "particle", "set_2_label"])
final_df.index = fname
final_df['prediction'] = final_df.idxmax(axis=1)
final_df['chip'] = final_df.index
print(final_df)
filepath = os.path.join("testing", 'results_ensemble.csv')
final_df.to_csv(filepath)

with torch.no_grad():
    get_scalar(Ensemble, classifier.support_set, classifier.query_set, 0, feat, val, dd)
    
get_acc("ensemble", "accuracy_winnow/acc_res.csv", 0, "data/"+sys.argv[1]+"/"+sys.argv[1]+"_truth_df.csv", support_dict, dd)
get_conf_mat("ensemble", "data/"+sys.argv[1]+"/"+sys.argv[1]+"_truth_df.csv", dd)
EOF
    git clone ssh://git@gitlab-data.pnnl.gov:2222/joecool/pychip-classifier.git
    cp -r ../../../patch/few_shot/pychip_classifier.py pychip-classifier/few_shot/
    mv model.py pychip-classifier/
    cd pychip-classifier
    mkdir mean-dist
    cp -r ../../../../patch/accuracy_winnow/ .
    cp -r ../../../../patch/data/ .
    echo "${reads}"
    python model.py "${molID}" "${ext}" "${feat_sel_ID}" "${reads}"
    cd ../
    out=${round}
    let "out=out+1"
    """
}
