import math
import io
import torch
import numpy as np
import matplotlib.pyplot as plt
import os
import ctypes
from ctypes import *

from PIL import Image
from torch.profiler import profile, record_function, ProfilerActivity
from torchvision import transforms
from torchvision.utils import save_image
from pytorch_msssim import ms_ssim
from compressai.zoo import bmshj2018_factorized

from nvidia.dali import pipeline_def, fn

image_path = "./image_to_compress" #"/qfs/projects/oddite/lenny/projects/compressai_profiling/TIFF1"
decomp_image_path = "./decompress_images"
result_path = "./result.txt"

def gpulz_compress():
    dll = ctypes.CDLL('./gpulz.so', mode=ctypes.RTLD_GLOBAL)
    func = dll.runCompression
    func.argtypes = [POINTER(c_uint32), c_uint32, c_char_p, c_void_p]
    return func

def gpulz_decompress():
    dll = ctypes.CDLL('./gpulz.so', mode=ctypes.RTLD_GLOBAL)
    func = dll.runDecompression
    func.argtypes = [POINTER(c_uint32), c_char_p, c_void_p]
    return func

def run_gpulz_comp(input_tensor, file_size, stream, compressed_file_name = 'compressed.bin'):
    # get input GPU pointer
    input_gpu_ptr = input_tensor.data_ptr()
    input_gpu_ptr = cast(input_gpu_ptr, ctypes.POINTER(c_uint32))

    file_size_c = c_uint32(file_size)

    b_string = compressed_file_name.encode('utf-8')
    b_string_ptr = c_char_p(b_string)

    stream_ptr = stream.cuda_stream
    stream_ptr = cast(stream_ptr, ctypes.c_void_p)

    gpulz_comp = gpulz_compress()
    gpulz_comp(input_gpu_ptr, file_size_c, b_string_ptr, stream_ptr)

def run_gpulz_decomp(output_tensor, stream, compressed_file_name = 'compressed.bin'):
    output_gpu_ptr = output_tensor.data_ptr()
    output_gpu_ptr = cast(output_gpu_ptr, ctypes.POINTER(c_uint32))

    b_string = compressed_file_name.encode('utf-8')
    b_string_ptr = c_char_p(b_string)

    stream_ptr = stream.cuda_stream
    stream_ptr = cast(stream_ptr, ctypes.c_void_p)

    gpulz_decomp = gpulz_decompress()
    gpulz_decomp(output_gpu_ptr, b_string_ptr, stream_ptr)

def python_gds_read():
    dll = ctypes.CDLL('./python_gds.so', mode=ctypes.RTLD_GLOBAL)
    func = dll.python_gdsRead
    func.argtypes = [c_char_p, c_void_p, c_size_t]
    return func

def run_python_gds_read(file_path, input_tensor, file_size):
    b_string = file_path.encode('utf-8')
    b_string_ptr = c_char_p(b_string)

    input_gpu_ptr = input_tensor.data_ptr()
    input_gpu_ptr = cast(input_gpu_ptr, ctypes.c_void_p)

    file_size_c = c_size_t(file_size)

    python_gds_read_func = python_gds_read()
    python_gds_read_func(b_string_ptr, input_gpu_ptr, file_size_c)

extensions=['.jpg', '.jpeg', '.png', '.gif', '.tiff']
directory = image_path
decomp_directory = decomp_image_path
for root, dirs, files in os.walk(directory):
    for file in files:
        if os.path.splitext(file)[1].lower() in extensions:
            new_size = 2000
            img_path2 = os.path.join(root, file)
            img_name = img_path2.split('/')[-1]
            img_name = img_name.split('.')[0]
            img = Image.open(img_path2).convert('RGB')
            img_resized = img.resize((new_size, new_size))
            img_resized.save(decomp_directory+"/"+img_name+'_resized.png')

            tensor_img = transforms.ToTensor()(img_resized).unsqueeze(0)
            torch.save(tensor_img, decomp_directory+"/"+img_name+'_resized.pt')

            numpy_array_img = tensor_img.numpy()
            numpy_array_img.tofile(decomp_directory+"/"+img_name+'_resized.bin')

            stream = torch.cuda.Stream() 
            with profile(activities=[ProfilerActivity.CPU, ProfilerActivity.CUDA], profile_memory=True, record_shapes=True) as prof:
                with record_function("model_inference"):
                    device = 'cuda' if torch.cuda.is_available() else 'cpu'         
                    net = bmshj2018_factorized(quality=1, pretrained=True).eval().to(device)
                    img = torch.load(decomp_directory+"/"+img_name+'_resized.pt').to(device)
                    y = net.g_a(img)
                    y_quantization = y.to(torch.int32)
                    file_size = y_quantization.shape[1] * y_quantization.shape[2] * y_quantization.shape[3] * 4
                    run_gpulz_comp(y_quantization, file_size, stream, decomp_directory+"/"+img_name+"_compressed.bin")
            print(prof.key_averages().table(sort_by="cuda_time_total", row_limit=10))
            prof.export_chrome_trace("gpulz.json")
