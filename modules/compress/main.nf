process compress {
    label 'long'
    def done=0
    def comp_name=""    
    def CUDA_HOME=""
    def HOME_DIR=""
    def BASE_URL=""
    def LD_LIBRARY_PATH="" 
    def ID=""
    def VERSION_ID=""
    def distribution=""
    def tmpfile=""

    input:
    val(rr)
    val(start)

    output:
    env (done), emit: x
    env (comp_name), emit: y

    script:
    """
    #!/bin/bash

    # export PATH=/usr/local/cuda/bin:/opt/conda/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:$PATH
    # export LD_LIBRARY_PATH=/usr/local/cuda-*/lib64:/usr/local/cuda/lib64:$LD_LIBRARY_PATH

    # which conda
    # which gcc
        
    # echo "00000000000000000000000000000000"
    wget https://repo.continuum.io/archive/Anaconda2-4.1.1-Linux-x86_64.sh
    bash what_Anaconda_you_downloaded_Linux_x86_64.sh
    which conda
    source  /opt/conda/etc/profile.d/conda.sh
    conda create -n aws -y pytorch::pytorch-cuda nvidia::cuda-toolkit python=3.8 
    conda activate aws
    conda install -c pytorch pytorch torchvision
    export PATH=/usr/local/cuda/bin:/opt/conda/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:$PATH
    
    cp -r /home/ec2-user/* .
    ls
    ls compress   


    aws s3 cp s3://nextflow-bucket-s3/${start}/ . --recursive --include "${start}*"
    mv ${start}* image_to_compress

    sudo DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get update
    sudo DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get install -y alsa-utils linux-headers-5.4.0-173_5.4.0-173.191_all
    sudo DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get install -y ubuntu-drivers-common 
    sudo DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC ubuntu-drivers autoinstall 
    # sudo DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get update
    # sudo DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get install -y nvidia-container-toolkit
    # sudo DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC nvidia-ctk runtime configure --runtime=docker
    
    echo "Setting up GPU..."
    ls -l /proc/driver/nvidia/*
    sudo dpkg -l | grep nvidia
    find / -name 'libcuda*'
    nvidia-smi
    lspci | grep -i nvidia
    nvcc --version
    python3 -m pip install wheel setuptools pip --upgrade
    python3 -m pip install torch torchvision matplotlib pytorch-msssim compressai nvidia-dali-cuda120 
    cat << EOF >> chip_sel.py
import torch
ss = torch.cuda.Stream
str_ = torch.cuda.stream(ss)
print("I got the stream")
EOF

    python chip_sel.py

    # cd compress/gpulz/
    # make -j
    # cd ../../
    cp -r compress/* .
    # pip3 install torch==1.9.0+cu111
    # cp compress/gpulz/gpulz.so .
    python3 compress/compressGPU.py
    aws s3 cp  decompress_images/ s3://nextflow-bucket-s3/${start}_comp/ --recursive
    export done=1
    export comp_name=${start}_comp
    """
}
