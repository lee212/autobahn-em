import numpy as np
from PIL import Image
import torch
import torchvision.transforms as transforms
from torchvision.models import resnet18
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import os

# Set device to CUDA if available, otherwise use CPU
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Load the pre-trained ResNet-18 model and remove the output layer
model = resnet18(pretrained=True)
model = torch.nn.Sequential(*list(model.children())[:-1])
model.to(device)
model.eval()

# Define a function to preprocess and extract features from a list of image files
def extract_features(image_files, model):
    features = []
    transform = transforms.Compose([
        transforms.Resize((224, 224)),  # ResNet-18 input size
        transforms.ToTensor(),
    ])

    for image_file in image_files:
        img = Image.open(image_file)

        # Ensure that the image has 3 channels (RGB)
        if img.mode != 'RGB':
            img = img.convert('RGB')

        img = transform(img).unsqueeze(0).to(device)
        feature_vector = model(img).squeeze().cpu().detach().numpy()
        features.append(feature_vector)
    return np.vstack(features)


# Directory paths containing ".tiff" files
directories = {
    'PRISM STO': './data/PRISM_HAADF/train/STO/',
    'Datahub STO': './data/PRISM_HAADF/test/STO/',
    'PRISM GE': './data/PRISM_HAADF/train/GE/',
    'Datahub GE': './data/PRISM_HAADF/test/GE/',
}

# Extract and store latent vectors for each directory
latent_vectors = {}
for dir_name, dir_path in directories.items():
    files = [os.path.join(dir_path, file) for file in os.listdir(dir_path) if file.lower().endswith('.tiff')]
    features = extract_features(files, model)
    latent_vectors[dir_name] = features

# Compute centroids for each directory
centroids = {}
for dir_name, features in latent_vectors.items():
    centroids[dir_name] = np.mean(features, axis=0)

# Calculate pairwise distances between centroids
centroid_distances = {}
for dir_name_1, centroid_1 in centroids.items():
    for dir_name_2, centroid_2 in centroids.items():
        if dir_name_1 != dir_name_2:
            distance = np.linalg.norm(centroid_1 - centroid_2)
            centroid_distances[f'{dir_name_1}-{dir_name_2}'] = distance

# Print centroid distances
for key, value in centroid_distances.items():
    print(f'Distance between centroids {key}: {value:.2f}')

# Apply t-SNE to visualize the combined latent vectors
tsne = TSNE(n_components=2, random_state=42)
tsne_result = tsne.fit_transform(np.vstack(list(latent_vectors.values())))

# Create a list of scatter plots for each directory
scatter_plots = []
a, b = 0, 0
for dir_name, features in latent_vectors.items():
    b = b + len(features)
    scatter_plot = plt.scatter(
        tsne_result[a:b, 0],
        tsne_result[a:b, 1],
        label=dir_name,
        alpha=0.7
    )
    a = b
    scatter_plots.append(scatter_plot)

# Combine all scatter plots into a single legend
plt.legend(handles=scatter_plots, loc='upper right')

plt.title('t-SNE Plot of Latent Space Vectors')
plt.xlabel('t-SNE Dimension 1')
plt.ylabel('t-SNE Dimension 2')
plt.show()
plt.savefig('compare.png')





# import numpy as np
# from PIL import Image
# import torch
# import torchvision.transforms as transforms
# from torchvision.models import resnet18
# from sklearn.manifold import TSNE
# import matplotlib.pyplot as plt
# import os

# # Set device to CUDA if available, otherwise use CPU
# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# # Load the pre-trained ResNet-20 model and remove the output layer
# model = resnet18(pretrained=True)
# model = torch.nn.Sequential(*list(model.children())[:-1])
# model.to(device)
# model.eval()

# # Define a function to preprocess and extract features from a list of image files
# def extract_features(image_files, model):
#     features = []
#     transform = transforms.Compose([
#         transforms.Resize((224, 224)),  # ResNet-18 input size
#         transforms.ToTensor(),
#     ])

#     for image_file in image_files:
#         img = Image.open(image_file)

#         # Ensure that the image has 3 channels (RGB)
#         if img.mode != 'RGB':
#             img = img.convert('RGB')

#         img = transform(img).unsqueeze(0).to(device)
#         feature_vector = model(img).squeeze().cpu().detach().numpy()
#         features.append(feature_vector)
#     return np.vstack(features)


# # Collect the list of ".tiff" image files in directories A and B
# dir_a = './data/PRISM_HAADF/STO/'
# dir_b = './data/PRISM_HAADF/test/STO/'
# dir_c = './data/PRISM_HAADF/GE/'
# dir_d = './data/PRISM_HAADF/test/GE/'

# label_a = 'PRISM HAADF STO'
# label_b = 'DataHub STO'
# label_c = 'PRISM HAADF GE'
# label_d = 'DataHub GE'

# figname = 'compare.png'

# files_a = [os.path.join(dir_a, file) for file in os.listdir(dir_a) if file.lower().endswith('.tiff')]
# files_b = [os.path.join(dir_b, file) for file in os.listdir(dir_b) if file.lower().endswith('.tiff')]
# files_c = [os.path.join(dir_c, file) for file in os.listdir(dir_c) if file.lower().endswith('.tiff')]
# files_d = [os.path.join(dir_d, file) for file in os.listdir(dir_d) if file.lower().endswith('.tiff')]

# # Extract features from both directories
# features_a = extract_features(files_a, model)
# features_b = extract_features(files_b, model)
# features_c = extract_features(files_a, model)
# features_d = extract_features(files_b, model)

# # Concatenate the features and apply t-SNE to visualize them
# concatenated_features = np.vstack((features_a, features_b, features_c, features_d))
# tsne = TSNE(n_components=2, random_state=42)
# tsne_result = tsne.fit_transform(concatenated_features)

# # Plot the t-SNE results
# plt.scatter(tsne_result[:len(features_a), 0], tsne_result[:len(features_a), 1], label=label_a, alpha=0.7)
# plt.scatter(tsne_result[len(features_a):len(features_a) + len(features_b), 0], tsne_result[len(features_a):len(features_a) + len(features_b), 1], label=label_b, alpha=0.7)
# plt.scatter(tsne_result[len(features_a) + len(features_b):len(features_a) + len(features_b) + len(features_c), 0], tsne_result[len(features_a) + len(features_b):len(features_a) + len(features_b) + len(features_c), 1], label=label_c, alpha=0.7)
# plt.scatter(tsne_result[len(features_a) + len(features_b) + len(features_c):, 0], tsne_result[len(features_a) + len(features_b) + len(features_c):, 1], label=label_d, alpha=0.7)
# plt.legend()
# plt.title('t-SNE Plot of Latent Space Vectors')
# plt.xlabel('t-SNE Dimension 1')
# plt.ylabel('t-SNE Dimension 2')
# plt.savefig(figname)
# plt.show()
