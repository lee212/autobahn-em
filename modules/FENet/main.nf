process train {
    label 'long'
    def done=0
    def MLFLOW_TRACKING_INSECURE_TLS=''
    input:
    val(rr)
    val(start)

    output:
    env (done), emit: x

    script:
    """
    #!/bin/bash

    export PATH=/opt/conda/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:$PATH

    which conda
    which gcc

    echo "00000000000000000000000000000000"
    source  /opt/conda/etc/profile.d/conda.sh
    conda create -n aws
    conda activate aws

    pip install --user torch-encoding==1.2
    pip install --user barbar==0.2.1

    cp -r /home/ec2-user/* .
    ls
    ls compress

    aws s3 cp s3://nextflow-bucket-s3/${start}/ . --recursive --include "${start}*"
    mv *.compressai image_to_decompress/

    python compress/decompress.py
    mkdir decompress_png
    mv decompress_images/*.png decompress_png

    export MLFLOW_TRACKING_INSECURE_TLS='true'
    python main.py --model=FENet --dataset=PRISM_HAADF --gamma=.99 --num_runs=1 --num_epochs=1
    export done=1
    """
}
