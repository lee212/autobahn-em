from PIL import Image
import os
import random
from pathlib import Path

def augment_and_save(image, output_directory, image_name, prefix, num_crops):
    for i in range(num_crops):
        left = random.randint(0, image.width - 224)
        upper = random.randint(0, image.height - 224)
        right = left + 224
        lower = upper + 224
        cropped_image = image.crop((left, upper, right, lower))
        
        # Save the cropped image
        cropped_image_path = os.path.join(output_directory, f"{image_name}_{prefix}{i+1}.tiff")
        cropped_image.save(cropped_image_path)
        print(f"Saved cropped image: {cropped_image_path}")

# Path to the parent directory containing subdirectories of .tiff images
parent_directory = './data/PRISM_HAADF/'  # Replace with the actual parent directory path

# Get a list of all subdirectories in the parent directory
subdirectories = [subdir for subdir in os.listdir(parent_directory) if os.path.isdir(os.path.join(parent_directory, subdir))]

# Loop through each subdirectory
for subdir in subdirectories:
    subdir_path = os.path.join(parent_directory, subdir)
    output_directory = os.path.join(parent_directory, 'train/' + subdir)
    test_output_directory = os.path.join(parent_directory, 'val/' + subdir)
    
    # Create the output directories if they don't exist
    Path(output_directory).mkdir(parents=True, exist_ok=True)
    Path(test_output_directory).mkdir(parents=True, exist_ok=True)
    
    # Get a list of all .tiff, .jpg files in the subdirectory
    image_files = [file for file in os.listdir(subdir_path) if file.lower().endswith('.tiff') or file.lower().endswith('.jpg')]
    
    # Loop through each image file and perform augmentation
    for image_file in image_files:
        image_path = os.path.join(subdir_path, image_file)
        image = Image.open(image_path)
        image_name = os.path.splitext(image_file)[0]  # Get the name of the image file without extension
        
        # Augment and save train images (4 crops)
        augment_and_save(image, output_directory, image_name, 'train', 10)
        
        # Augment and save test images (1 crop)
        augment_and_save(image, test_output_directory, image_name, 'val', 10)
