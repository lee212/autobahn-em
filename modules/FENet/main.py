import os
os.environ['MLFLOW_TRACKING_INSECURE_TLS'] = 'true'
from opts import parse_opts
from Info import Datasets_Info
from prepare_dataloader import Prepare_DataLoaders
from utils import *
import torch.nn as nn
import torch
from train import train_model
from test import test_model
import mlflow
from torch.backends import cudnn
from datetime import datetime
import pickle



# cudnn.benchmark = True            # if benchmark=True for accelerating, deterministic will be False
# cudnn.deterministic = False

print(cudnn.benchmark)
print(cudnn.deterministic)

if __name__ == '__main__':
    opt_Parser = parse_opts()
    params_dict = vars(opt_Parser.parse_args())
    #logfile name
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d__%H-%M-%S/")
    log_dir = os.path.join(params_dict['dir_log'], params_dict['scope'], params_dict['dataset'],
                               params_dict['model'] + '_' + params_dict['backbone'][-2:] + '_' + params_dict['save_name'] + '_' + timestamp)
    os.makedirs(os.path.dirname(log_dir),exist_ok=True)
    #setup log_file
    logfile = f"output.log"
    logger = get_logger(log_dir,logfile)
    logger.info("================ LOGGING =================")
    opt_Parser = Parser(opt_Parser,logger=logger,log_dir=log_dir)
    opt = opt_Parser.get_arguments()
    num_Runs = Datasets_Info['splits'][opt.dataset]
    num_Runs = opt.num_runs if opt.num_runs else num_Runs
    opt.n_classes = Datasets_Info['num_classes'][opt.dataset]

    
    

    opt_Parser.write_args()
    opt_Parser.print_args()
    logger.info('-->> dataset:{} | model:{} | backbone:{} <<--'.format(opt.dataset, opt.model, opt.backbone))
    logger.info(f"model path:{log_dir}{opt.model}.pth")

    if opt.gpu_ids and torch.cuda.is_available():
        device = torch.device("cuda:%d" % opt.gpu_ids[0])
        torch.cuda.set_device(opt.gpu_ids[0])
        torch.cuda.manual_seed(opt.seed)
    else:
        device = torch.device("cpu")
        torch.manual_seed(opt.seed)
    
    logger.info(f"Device: {str(device)}")

    mlflow.set_experiment('FENet-'+opt.dataset)

    for split in range(0, num_Runs):
        logger.info('Run_{}'.format(split + 1))
        mlflow.start_run(run_name='_'.join([str(opt.lr).replace('.','-'), opt.model, opt.dataset, 'split{}'.format(split)]))
        mlflow.log_param("lr",opt.lr)
        mlflow.log_param("backbone",opt.backbone)
        mlflow.log_param("train_BS",opt.train_BS)
        mlflow.log_param("val_BS",opt.val_BS)
        mlflow.log_param("test_BS",opt.test_BS)
        mlflow.log_param("dataset",opt.dataset)
        mlflow.log_param("model",opt.model)
        mlflow.log_param("lr_scheduler",opt.scheduler)
        mlflow.log_param("lr_step",opt.lr_step)
        mlflow.log_param("save_name",opt.save_name)
        mlflow.log_param("epochs",opt.num_epochs)
        mlflow.log_param("seed",opt.seed)
        mlflow.log_param("dim",opt.dim)

        dataloaders_dict = Prepare_DataLoaders(opt, split, input_size=(224, 224))
        model = initialize_model(opt).to(device)
        # for k,v in model.state_dict().items():
        #     print(k)
        # print(model)
        # import pdb
        # pdb.set_trace()
        if opt.resume:
            previous_model_weights = torch.load(opt.resume_path)
            model.load_state_dict(previous_model_weights)
            epoch_start = opt.begin_epoch
        else:
            epoch_start = 0        
        # num_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
        # print("Number of parameters: %d" % (num_params))
        optimizer = get_Optimizer(opt, model)
        scheduler = get_Scheduler(opt, optimizer)
        criterion = nn.CrossEntropyLoss()
        # if opt.gpu_ids and torch.cuda.is_available():
        #     model = torch.nn.DataParallel(model)
        if opt.train_need:
            train_dict = train_model(model, dataloaders_dict, criterion, optimizer, device, opt.num_epochs, epoch_start, scheduler,logger)
            mlflow.log_metrics({"Best_Accuracy":float(train_dict['best_val_acc']), "Best_Epoch":int(train_dict['best_epoch'])})
        if opt.val_need:
            val_dict = test_model(model, dataloaders_dict['val'], device,logger)
        if opt.test_need:
            test_dict = test_model(model, dataloaders_dict['test'], device,logger, "Test")
        if opt.train_need and opt.val_need and opt.save_result:
            save_results(train_dict, val_dict, split, opt)
        mlflow.end_run()
    #if opt.train_need:
        #get_result(opt,logger)
    logger.info('-->> Train Done <<--')
    
    #Save model
    opt_path = f"{log_dir}opt.pth"
    model_path = f"{log_dir}{opt.model}.pth"
    torch.save(model.state_dict(), model_path)
    with open(opt_path,'wb') as file:
        pickle.dump(opt,file)




